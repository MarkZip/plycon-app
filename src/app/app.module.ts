import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes, CanActivate} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OrderService} from './services/order.service';
import {AuthGuard} from './services/authguard.service';
import {AdminGuard} from './services/adminguard.service';
import {AppComponent} from './app.component';
import {FormComponent} from './form/form.component';
import {HomeComponent} from './home/home.component';
import {OrdersComponent} from './orders/orders.component';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ActivateComponent } from './activate/activate.component';
import { RegisterComponent } from './register/register.component';


const appRoutes: Routes = [

  {path: '', component: HomeComponent},
  {path: 'referrals', component: OrdersComponent, canActivate: [AuthGuard]},
  {path: 'referral', component: FormComponent, canActivate: [AuthGuard]},
  {path: 'referral/:id', component: FormComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent, canActivate: [AdminGuard] },
  {path: 'newpassword', component: ResetPasswordComponent,canActivate: [AuthGuard] },
  {path: 'newpassword/:token', component: ResetPasswordComponent },
  {path: 'activate', component: ActivateComponent, canActivate: [AuthGuard]},
  {path: 'activate/:token', component: ActivateComponent },



];


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    HomeComponent,
    OrdersComponent,
    LoginComponent,
    ResetPasswordComponent,
    ActivateComponent,
    RegisterComponent
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
	BrowserAnimationsModule,
	MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(appRoutes, {useHash: false}),
	   AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDpIehnaRSxhKXTHgQxgjonm3cHOQ4msKo',
	  libraries: ["places", "geometry"]
    })

  ],
  
  schemas: [ NO_ERRORS_SCHEMA ],
  
  providers: [OrderService, AuthGuard, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
