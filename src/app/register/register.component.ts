import { Component} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {OrderService} from '../services/order.service';
import {Router} from '@angular/router'; 

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

 email;
 emailadd;
 password;
 namevalid;
 phonevalid;
 name;
 phone;

  constructor(public router : Router, public ordersvc : OrderService) { 
  this.email = new FormControl('', [Validators.email, Validators.required]);
  this.namevalid = new FormControl('', [Validators.required]);
  this.phonevalid = new FormControl('', [Validators.required]);
  }
  
  generatePassword() {
  let text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

  
  public signUpUser() {
     
	 this.password = this.generatePassword();
   
      this.ordersvc.signup(this.emailadd, this.password, this.name,this.phone)
	   .subscribe(
	  (response) => {
    alert("Partner registration successful! Please inform partner to check their email for account activation instructions")
    this.router.navigateByUrl('/referrals')
	  },
	  (error) => alert('Partner details are not submitted. Please register again')
	  );

       
	  
  }

}
