import { Injectable, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import {
  Router,
  RouterModule,
  ActivatedRoute,
  ParamMap,
  RouterLink
} from "@angular/router";
import { environment } from "../../environments/environment";

@Injectable()
export class OrderService {
  constructor(
    private http: HttpClient,
    private _http: Http,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  //GENERAL

  public getAccessToken() {
    const token = localStorage.getItem("accessToken");
    if (token) {
      return "Bearer " + token;
    }
    return token;
  }

  public getOptions(): RequestOptions {
    const token = this.getAccessToken();
    return new RequestOptions({
      headers: new Headers({
        Authorization: token
      })
    });
  }

  public login(email: string, password: string): Observable<Response> {
    const data = {
      email: email,
      password: password
    };
    return this._http
      .post(environment.apiBaseUrl + "/api/v1/auth/local", data)
      .map(res => {
        const user = res.json();
        console.log('USER', user)
        localStorage.setItem("accessToken", user.token);
        localStorage.setItem("myUserId", user.userId);
        return user;
      });
  }

  public logout() {
    // this._router.navigate(['']);
    localStorage.clear();
  }

  public loggedIn() {
    const token = localStorage.getItem("accessToken");
    if (!!token && token != "undefined") {
      return true;
    }
    return false;
  }

  public forgotPassword(email) {
    const data = {
      email: email
    };
    return this.http
      .post(environment.apiBaseUrl + "/api/v1/users/forgotpassword", data)
      .map(res => {
        return res;
      });
  }

  public resetPassword(resetToken, newPassword): Observable<any> {
    const body = {
      resetToken: resetToken,
      newPassword: newPassword
    };
    return this._http
      .patch(
        environment.apiBaseUrl + "/api/v1/users/me/password",
        body,
        this.getOptions()
      )
      .map(res => {
        return res;
      });
  }

  //PARTNER

  getOrder(id) {
    const url = environment.apiBaseUrl + "/api/v1/referral/" + id;
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.get(url, options);
  }

  createOrder(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, obj, options);
  }

  updateOrder(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.patch(url, obj, options);
  }

  deleteOrder(id) {
    const url = environment.apiBaseUrl + "/api/v1/referral/" + id;
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.delete(url, options);
  }

  getOrders() {
    const url = environment.apiBaseUrl + "/api/v1/referral";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.get(url, options);
  }

  completeOrder(referralId) {
    const url = environment.apiBaseUrl + "/api/v1/referral/complete";
    const token = this.getAccessToken();
    const body = {
      referralId: referralId
    };
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, body, options);
  }

  calculateOrder(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/pricing/calculate";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, obj, options);
  }

  //ADMIN

  adminGetOrders() {
    const url = environment.apiBaseUrl + "/api/v1/referral/admin";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.get(url, options);
  }

  adminGetOrder(id) {
    const url = `${environment.apiBaseUrl}/api/v1/referral/admin/${id}`;
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.get(url, options);
  }

  approveCharge(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/admin/approve";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, obj, options).map(res => {
      const response = res;
      return response;
    });
  }

  rejectCharge(referralId) {
    const url = environment.apiBaseUrl + "/api/v1/referral/admin/reject";
    const body = {
      referralId: referralId
    };
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, body, options).map(res => {
      const response = res;
      return response;
    });
  }

  additionalCharge(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/admin/charge";

    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, obj, options).map(res => {
      const response = res;
      return response;
    });
  }

  public refundPaidOrder(obj) {
    const url = environment.apiBaseUrl + "/api/v1/referral/admin/refund";
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    return this.http.post(url, obj, options).map(res => {
      const response = res;
      return response;
    });
  }

  // Other - will probably not use

  public signup(
    email: string,
    password: string,
    name: string,
    phone: string
  ){
    const token = this.getAccessToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: token
      })
    };
    const data = {
      email,
      password,
      name,
      phone
    };
    return this.http
    .post(environment.apiBaseUrl + "/api/v1/users/createByAdmin", data, options)
    .map(res => {
      return res;
    });
  }

  public activate(activateToken) {
    const data = {
      activateToken: activateToken
    };
    return this._http
      .put(`https://api.snapfindz.com/users/activation`, data)
      .map(res => {
        return res;
      });
  }

  public reactivate() {
    const token = this.getAccessToken();
    const headers = new Headers({
      Authorization: token
    });
    return this._http
      .put(`https://api.snapfindz.com/users/reactivation`, headers)
      .map(res => {
        return res.json();
      });
  }
}
