import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {OrderService} from './order.service';


@Injectable()

export class AuthGuard implements CanActivate{


   constructor(private ordersvc: OrderService, private router: Router){}

  
   canActivate(): boolean {
   
    if(!this.ordersvc.loggedIn()){
	  alert("Please login to your account to view this page")
	  this.router.navigate(['']);
	  return false;
	}
    
	 return true;
   
   }











}