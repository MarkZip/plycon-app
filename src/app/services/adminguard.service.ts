import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { OrderService } from "./order.service";

import * as jwt_decode from "jwt-decode";

@Injectable()
export class AdminGuard implements CanActivate {
  userRole;
  constructor(private ordersvc: OrderService, private router: Router) {}

  canActivate(): boolean {
    if (!this.ordersvc.loggedIn()) {
      alert("Please login to your account to view this page");
      this.router.navigate([""]);
      return false;
    }
    var token = localStorage.getItem("accessToken");
    var decoded = jwt_decode(token);
    this.userRole = decoded.role;
    if (this.userRole === "superAdmin") {
      return true;
    } else {
      alert("You are not authorized to view this page");
      this.router.navigate([""]);
      return false;
    }
  }
}
