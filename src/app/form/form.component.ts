import {
  Component,
  OnInit,
  OnDestroy,
  NgZone,
  ElementRef,
  ViewChild
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OrderService } from "../services/order.service";
import {} from "@types/googlemaps";
import { MapsAPILoader } from "@agm/core";
import * as jwt_decode from "jwt-decode";

declare var google: any;

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.css"],
  providers: [OrderService]
})
export class FormComponent implements OnInit {
	userRole;
	loading = false;
  //google address autocomplete
  public street;
  public city;
  public state;
  public zipcode;
  public originAdd;
  public destAdd;
  public originlat;
  public originlng;
  public destlat;
  public destlng;
  public distanceInMiles: any = "";
  public bill_street;
  public bill_city;
  public bill_state;
  public bill_zipcode;
  @ViewChild("myDiv") myDiv: ElementRef;
  @ViewChild("autoaddress") autoaddress: ElementRef;
  //credit card
  public cardNumber;
  public expiryMonth;
  public expiryYear;
  public cvc;
  public cardName;
  public ccList: any = [];
  public selectedCard: any;
  public addNewCard;
  stripemessage;
  sub;
  activeCard;
  //Additional charge - credit card
  public refundAmount;
  public chargeAmount;
  public chargeCardNumber;
  public chargeExpiryMonth;
  public chargeExpiryYear;
  public chargeCvc;
  public chargeCardName;
  public chargeBill_street;
  public chargeBill_city;
  public chargeBill_state;
  public chargeBill_zipcode;
  public chargeCCList: any = [];
  public chargeSelectedCard: any;
  chargeActiveCard;
  message;
  order: any;
  public quote: string;
  public name: string;
  public originName: string;
  public partnerName: string;
  public total: string;
  public id: string;
  public inventory: any;
  public invModel: any;
  public orders: any;
  public orderId: any;
  public sourceId: any;
  public sameBillingInfo: any;
  private timer: any;
  //CalculatedFields
  public totalCube: any;
  public mileageRate: any;
  public transportationCharge: any;
  public fuelCharge: any;
  public itemWorthTotal: any;
  public q1: any;
  public q2: any;
  public pq1Total: any;
  public total1: any;
  public total2: any;
  public finalTotal: any;
  OrderForm: FormGroup;
  ShippingForm: FormGroup;
  PaymentForm: FormGroup;
  InventoryForm: FormGroup;
  phonePattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  isValidUserFormSubmitted = null;
  isValidBillFormSubmitted = null;
  isFormValid = true;
  isInventoryValid = true;

  constructor(
    private fb: FormBuilder,
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
    private route: ActivatedRoute,
    private router: Router,
    private ordersvc: OrderService
  ) {
    this.inventory = [];
    //Order / Root model
    this.OrderForm = this.fb.group({
      totalCube: ["", Validators.required],
      totalDistance: [this.distanceInMiles, [Validators.required]],
      mileageRate: ["", Validators.required],
      transportationCharge: ["", Validators.required],
      fuelCharge: ["", Validators.required],
      itemWorth: [""],
      itemWorthProtection: [""],
      originDestinationFee: [""],
      extraMan: [""],
      complexSetup: [""],
      simpleSetup: [""],
      addFlightStairs: [""],
      cratingItem: [""],
      extraBlanketWrap: [""],
      q1: ["", Validators.required],
      q2: ["", Validators.required],
      qSelected: ["q1", Validators.required],
      pq1: ["", Validators.required],
      total1: ["", Validators.required],
      total2: ["", Validators.required],
      finalTotal: [""]
    });
    this.ShippingForm = this.fb.group({
      partnerName: [""],
      originName: [
        "",
        Validators.compose([Validators.minLength(3), Validators.required])
      ],
      originPhone: [
        "",
        Validators.compose([
          Validators.pattern(this.phonePattern),
          Validators.required
        ])
      ],
      originAddress: ["", Validators.required],
      conName: [
        "",
        Validators.compose([Validators.minLength(3), Validators.required])
      ],
      conPhone: [
        "",
        Validators.compose([
          Validators.pattern(this.phonePattern),
          Validators.required
        ])
      ],
      conAddress: ["", Validators.required],
      email: [
        "",
        Validators.compose([
          Validators.pattern(this.emailPattern),
          Validators.required
        ])
      ],
      specialRequests: [""]
    });
    this.PaymentForm = this.fb.group({
      ccNumber: ["", Validators.required],
      month: ["", Validators.required],
      year: ["", Validators.required],
      ccv: ["", Validators.required],
      ccName: ["", Validators.required],
      selectCard: [""],
      address: ["", Validators.required],
      city: ["", Validators.required],
      state: ["", Validators.required],
      zip: ["", Validators.required],
      tel: ["", Validators.required]
    });
    this.InventoryForm = this.fb.group({
      quantity: ["", Validators.required],
      description: ["", Validators.required],
      length: ["", Validators.required],
      width: ["", Validators.required],
      height: ["", Validators.required]
    });
    this.invModel = {
      quantity: null,
      description: null,
      length: null,
      width: null,
      height: null
    };
  }

  ngOnInit() {
		this.loading = true;
    this.getAccessToken();
    //google address Autocomplete - shipping address
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        <HTMLInputElement>document.getElementById("autocomplete2"),
        {
          types: ["address"]
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          var location = place.geometry.location;
          this.destlat = location.lat();
          this.destlng = location.lng();
          console.log(this.destlat, this.destlng);
          this.destAdd = place.formatted_address;
          var addressArray = [];
          var componentForm = {
            street_number: "short_name",
            route: "long_name",
            locality: "long_name",
            administrative_area_level_1: "short_name",
            postal_code: "short_name"
          };
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              addressArray.push(val);
            }
          }
          console.log(addressArray);
          this.street = addressArray[0] + " " + addressArray[1];
          this.city = addressArray[2] || "";
          this.state = addressArray[3] || "";
          this.zipcode = addressArray[4] || "";
          this.ShippingForm.value.conAddress =
            this.street +
            " " +
            this.city +
            ", " +
            this.state +
            " " +
            this.zipcode;
          this.clickDistance();
        });
      });
    });
    //google address Autocomplete - form billing address
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        <HTMLInputElement>document.getElementById("autocomplete4"),
        {
          types: ["address"]
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          var addressArray = [];
          var componentForm = {
            street_number: "short_name",
            route: "long_name",
            locality: "long_name",
            administrative_area_level_1: "short_name",
            postal_code: "short_name"
          };
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              addressArray.push(val);
            }
          }
          console.log(addressArray);
          this.bill_street = addressArray[0] + " " + addressArray[1];
          this.bill_city = addressArray[2] || "";
          this.bill_state = addressArray[3] || "";
          this.bill_zipcode = addressArray[4] || "";
        });
      });
    });
    //google address Autocomplete - charge modal billing address
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        <HTMLInputElement>document.getElementById("autocomplete5"),
        {
          types: ["address"]
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          var addressArray = [];
          var componentForm = {
            street_number: "short_name",
            route: "long_name",
            locality: "long_name",
            administrative_area_level_1: "short_name",
            postal_code: "short_name"
          };
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              addressArray.push(val);
            }
          }
          console.log(addressArray);
          this.chargeBill_street = addressArray[0] + " " + addressArray[1];
          this.chargeBill_city = addressArray[2] || "";
          this.chargeBill_state = addressArray[3] || "";
          this.chargeBill_zipcode = addressArray[4] || "";
        });
      });
    });
    //google address Autocomplete - refund modal billing address
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        <HTMLInputElement>document.getElementById("autocomplete6"),
        {
          types: ["address"]
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          var addressArray = [];
          var componentForm = {
            street_number: "short_name",
            route: "long_name",
            locality: "long_name",
            administrative_area_level_1: "short_name",
            postal_code: "short_name"
          };
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              addressArray.push(val);
            }
          }
          console.log(addressArray);
          this.chargeBill_street = addressArray[0] + " " + addressArray[1];
          this.chargeBill_city = addressArray[2] || "";
          this.chargeBill_state = addressArray[3] || "";
          this.chargeBill_zipcode = addressArray[4] || "";
        });
      });
    });
    //google address Autocomplete - origin address
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        <HTMLInputElement>document.getElementById("autocomplete3"),
        {
          types: ["address"]
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          var location = place.geometry.location;
          this.originlat = location.lat();
          this.originlng = location.lng();
          console.log(this.originlat, this.originlng);
          this.originAdd = place.formatted_address;
          var addressArray = [];
          var componentForm = {
            street_number: "short_name",
            route: "long_name",
            locality: "long_name",
            administrative_area_level_1: "short_name",
            postal_code: "short_name"
          };
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              addressArray.push(val);
            }
          }
          console.log(addressArray);
          this.street = addressArray[0] + " " + addressArray[1];
          this.city = addressArray[2];
          this.state = addressArray[3];
          this.zipcode = addressArray[4];
          this.ShippingForm.value.originAddress =
            this.street +
            " " +
            this.city +
            ", " +
            this.state +
            " " +
            this.zipcode;
          this.clickDistance();
        });
      });
    });
    const self = this;
    if (self.route.params) {
      self.sub = self.route.params.subscribe(params => {
        self.id = params.id;
      });
    }
    if (self.id) {
      this.getOrder(self.id);
      this.orderId = self.id;
    } else {
      this.addInvItem();
    }
    self.order = {};
    let selectedID;
    let selectedQuote;
    let selectedName;
    let selectedTotal;
    for (let i = 0; i < self.order.length; i++) {
      if (self.order[i].id == self.id) {
        selectedID = self.order[i].id;
        selectedQuote = self.order[i].quote;
        selectedName = self.order[i].name;
        selectedTotal = self.order[i].total;
        console.log(selectedID, selectedQuote, selectedName, selectedTotal);
        self.quote = selectedQuote;
        self.name = selectedName;
        self.total = selectedTotal;
      }
    }
  }
  trackByFn(index: any, item: any) {
    return index;
  }

  calculateOrder() {
    console.log("calculating");
    try {
      clearTimeout(this.timer);
    } catch (err) {
      console.log(err);
    }
    this.timer = setTimeout(() => {
      let sendObject = this.compileOrderForms();
      this.ordersvc.calculateOrder(sendObject).subscribe(
        result => {
          this.fillOrderForms(result);
        },
        err => {
          console.log(err);
        }
      );
    }, 0);
  }

  getAccessToken() {
    var token = localStorage.getItem("accessToken");
    var decoded = jwt_decode(token);
    this.userRole = decoded.role;
    console.log(this.userRole);
  }

  getOrder(id) {
    if (this.userRole && this.userRole === "superAdmin") {
      this.ordersvc.adminGetOrder(id).subscribe(
        result => {
          if (!result["_id"]) {
            this.router.navigateByUrl("/referral");
          }
          // this.ccList = result['cards'] ?
          // result['cards']['data'] : [];
          // this.chargeCCList = result['cards'] ?
          // result['cards']['data'] : [];
          // if(this.ccList) {
          // 	this.selectedCard = result['cards']['data'][0];
          // }
          this.order = result;
          console.log("ADMIN ORDER", result);
          this.fillOrderForms(this.order);
          if (result["inventoryItems"].length < 1) {
            this.addInvItem();
          }
					this.calculateOrder();
					this.loading = false;
        },
        err => {
          console.log(err);
					this.router.navigateByUrl("/referrals");
					this.loading = false;
        }
      );
    } else {
      this.ordersvc.getOrder(id).subscribe(
        result => {
          if (!result["_id"]) {
            this.router.navigateByUrl("/referral");
          }
          // this.ccList = result['cards'] ? result[
          // 'cards']['data'] : [];
          // this.chargeCCList = result['cards'] ?
          // result['cards']['data'] : [];
          //  if(this.ccList) {
          // 	this.selectedCard = result['cards']['data'][0];
          // }
          this.order = result;
          console.log("PARTNER ORDER", this.order);
          this.fillOrderForms(result);
          if (result["inventoryItems"].length < 1) {
            this.addInvItem();
          }
					this.calculateOrder();
					this.loading = false;
        },
        err => {
          console.log(err);
					this.router.navigateByUrl("/referrals");
					this.loading = false;
        }
      );
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getFormData() {
    let orderObj = this.OrderForm.value;
  }

  logout() {
    localStorage.clear();
  }

  cancel() {
    var ans = confirm("Are you sure you want to cancel changes?");
    if (ans) {
      this.router.navigateByUrl("/referrals");
    }
  }

  closePage() {
    var ans = confirm("Are you sure you want to close this form?");
    if (ans) {
      this.router.navigateByUrl("/referrals");
    }
  }

  deleteProfile() {
    if (this.orderId) {
      var ans = confirm("Are you sure you want to delete this profile?");
      if (ans) {
        this.ordersvc.deleteOrder(this.orderId).subscribe(
          result => {
            this.router.navigateByUrl("/referrals");
            console.log(result);
          },
          err => {
            console.log(err);
          }
        );
      }
    }
  }

  callStripe(cb) {
    if (
      this.cardNumber &&
      this.expiryMonth &&
      this.expiryYear &&
      this.cvc &&
      this.cardName &&
      this.bill_street &&
      this.bill_city &&
      this.bill_state &&
      this.bill_zipcode
    ) {
      (<any>window).Stripe.card.createToken(
        {
          number: this.cardNumber,
          exp_month: this.expiryMonth,
          exp_year: this.expiryYear,
          cvc: this.cvc,
          name: this.cardName,
          address_line1: this.bill_street,
          address_city: this.bill_city,
          address_state: this.bill_state,
          address_zip: this.bill_zipcode
        },
        (status: number, response: any) => {
          this.ngZone.run(() => {
            if (status === 200) {
              console.log(response);
              this.sourceId = response.id;
              console.log(this.sourceId);
              cb(this.sourceId);
            } else {
              console.log(response.error.message);
              this.stripemessage = response.error.message;
            }
            //cb();
          });
        }
      );
    } else {
      cb();
    }
  }

  onSave() {
    var self = this;

    this.callStripe(function(sourceId) {
      self.isValidUserFormSubmitted = false;

      let sendObject = self.compileOrderForms();

      if (sourceId) {
        sendObject["source"] = sourceId;
      }

      console.log('SEND ObJ', sendObject);
      //decide if create or update

      if (self.orderId) {
        sendObject["id"] = self.orderId;
        self.ordersvc.updateOrder(sendObject).subscribe(
          result => {
            console.log(result);
            self.isValidUserFormSubmitted = true;
            alert("Form saved successfully!");
            self.router.navigateByUrl("/referrals");
          },
          err => {
            alert("Please contact Plycon Micro Move to complete your referral");
            console.log(err);
          }
        );
      } else {
        self.ordersvc.createOrder(sendObject).subscribe(
          result => {
            console.log(result);
            self.isValidUserFormSubmitted = true;
            alert("Form saved successfully!");
            self.router.navigateByUrl("/referrals");
          },
          err => {
            alert("Please contact Plycon Micro Move to complete your referral");
            console.log(err);
          }
        );
      
      }
    });
  }

  generateQuote() {
    console.log(this.inventory);
    this.inventory.filter(i => {
      if (
        Object.values(i).indexOf(null) > -1 ||
        Object.values(i).indexOf("") > -1
      ) {
        this.isInventoryValid = false;
      } else {
        this.isInventoryValid = true;
      }
    });
    this.isValidUserFormSubmitted = false;
    this.isFormValid = false;
    console.log("SHIPPING FORM", this.ShippingForm.status);
    if (this.ShippingForm.status !== "VALID") {
      alert(
        "Please check all fields and enter all required (**) shipping form information"
      );
      return;
    }
    console.log("INVENTORY FORM COMPLETE", this.isInventoryValid);
    if (this.isInventoryValid === false) {
      alert(
        "Please check all fields and enter all required (**) inventory form information"
      );
      return;
    }
    console.log("ORDER FORM", this.OrderForm.status);
    if (this.OrderForm.status !== "VALID") {
      alert(
        "Please check all fields and enter all required (**) shipment form information"
      );
      return;
    }
    console.log("PAYMENT SRC", this.ccList.length);
    if (this.ccList.length === 0) {
      alert(
        "Please check all fields and enter all required (**) payment form information"
      );
      return;
    }

    var self = this;

    this.callStripe(function(sourceId) {
      self.isValidUserFormSubmitted = false;

      let sendObject = self.compileOrderForms();

      if (sourceId) {
        sendObject["source"] = sourceId;
      }

      console.log(sendObject);
      //decide if create or update

      if (self.orderId) {
        sendObject["id"] = self.orderId;
        self.ordersvc.updateOrder(sendObject).subscribe(
          result => {
            console.log(result);
            self.ordersvc.completeOrder(self.orderId).subscribe(
              result => {
                console.log(result);
                self.isValidUserFormSubmitted = true;
                alert(
                  "Thank you for the Referral. PlyconMicroMove Processing Shipment."
                );
                self.router.navigateByUrl("/referrals");
              },
              err => {
                console.log(err);
              }
            );
          },
          err => {
            alert("Please contact Plycon Micro Move to complete your referral");
            console.log(err);
          }
        );
      } else {
        self.ordersvc.createOrder(sendObject).subscribe(
          result => {
            console.log(result);
            self.ordersvc.completeOrder(result["_id"]).subscribe(
              result => {
                console.log(result);
                self.isValidUserFormSubmitted = true;
                alert(
                  "Thank you for the Referral. PlyconMicroMove Processing Shipment."
                );
                self.router.navigateByUrl("/referrals");
              },
              err => {
                console.log(err);
              }
            );
          },
          err => {
            alert("Please contact Plycon Micro Move to complete your referral");
            console.log(err);
          }
        );
      }
      self.isValidUserFormSubmitted = true;
      alert("Form saved successfully!");
    });
  }

  resetPage() {
    this.OrderForm.reset();
    this.ShippingForm.reset();
    this.PaymentForm.reset();
    this.inventory = [];
    this.addInvItem();
    //reset calc
    this.totalCube = null;
    this.mileageRate = null;
    this.transportationCharge = null;
    this.fuelCharge = null;
    this.itemWorthTotal = null;
    this.q1 = null;
    this.q2 = null;
    this.pq1Total = null;
    this.total1 = null;
    this.total2 = null;
    this.finalTotal = null;
    this.sameBillingInfo = false;
    this.router.navigateByUrl("/referrals");
  }

  onPay(value) {
    //TODO remove intercept to add back validations
    this.isValidBillFormSubmitted = false;
    this.isValidBillFormSubmitted = true;
    alert("Form submitted successfully!");
    console.log(JSON.stringify(value));
  }

  getFormAttribute(formName, attribute) {
    return this[formName].get(attribute);
  }

  logInventory() {
    console.log(this.inventory);
  }

  addInvItem() {
    console.log(this.inventory);
    let pushObj = this.jsonCopy(this.invModel);
    this.inventory.push(pushObj);
    console.log(this.inventory);
    this.loading = false;
  }

  removeInvItem(index) {
    this.inventory.splice(index, 1);
    this.calculateOrder();
  }

  jsonCopy(x) {
    return JSON.parse(JSON.stringify(x));
  }

  fillFormValue(formName, attribute, value) {
    try {
      this[formName].controls[attribute].setValue(value);
    } catch (err) {}
  }

  fillOrderForms(fullObj) {
    //add calculated items
    Object.keys(fullObj.shippingDetails).map(attribute => {
      this.fillFormValue(
        "ShippingForm",
        attribute,
        fullObj.shippingDetails[attribute]
      );
    });
    //Object.keys(fullObj.billing).map((attribute) => {
    //  this.fillFormValue('BillingForm', attribute, fullObj.billing[attribute]);
    // });
    // Object.keys(fullObj.payment).map((attribute) => {
    //   this.fillFormValue('PaymentForm', attribute, fullObj.payment[attribute]);
    // });
    Object.keys(fullObj).map(attribute => {
      this.fillFormValue("OrderForm", attribute, fullObj[attribute]);
    });
    if (this.inventory && this.inventory.length) {
      for (var i = 0; i < this.inventory.length; i++) {
        this.inventory[i].cube = fullObj.inventoryItems[i].cube;
      }
    } else {
      this.inventory = fullObj.inventoryItems;
    }
    this.totalCube = fullObj.totalCube;
    this.mileageRate = fullObj.mileageRate;
    this.transportationCharge = fullObj.transportationCharge;
    this.fuelCharge = fullObj.fuelCharge;
    this.itemWorthTotal = fullObj.itemWorthTotal;
    this.q1 = fullObj.q1;
    this.q2 = fullObj.q2;
    this.pq1Total = fullObj.pq1Total;
    this.total1 = fullObj.total1;
    this.total2 = fullObj.total2;
    this.finalTotal = fullObj.finalTotal;
    this.finalTotal = fullObj.finalTotal;
    this.sameBillingInfo = fullObj.sameBillingInfo;
  }

  compileOrderForms() {
    const sendObj = this.jsonCopy(this.OrderForm.value);
    sendObj.shippingDetails = this.jsonCopy(this.ShippingForm.value);
    sendObj.inventoryItems = this.jsonCopy(this.inventory);
    sendObj.payment = this.jsonCopy(this.PaymentForm.value);
    sendObj.totalCube = this.totalCube;
    sendObj.mileageRate = this.mileageRate;
    sendObj.transportationCharge = this.transportationCharge;
    sendObj.fuelCharge = this.fuelCharge;
    sendObj.itemWorthTotal = this.itemWorthTotal;
    sendObj.q1 = this.q1;
    sendObj.q2 = this.q2;
    sendObj.pq1Total = this.pq1Total;
    sendObj.total1 = this.total1;
    sendObj.total2 = this.total2;
    sendObj.finalTotal = sendObj.qSelected === "q2" ? this.total2 : this.total1;
    sendObj.sameBillingInfo = this.sameBillingInfo;
    sendObj.id = this.orderId;
    return sendObj;
  }

  isString(val) {
    if (typeof val === "string") {
      return true;
    } else {
      return false;
    }
  }

  calcDistance() {
    this.calculateOrder();
    let directionsService = new google.maps.DirectionsService();
    var distanceInput = <HTMLInputElement>document.getElementById("distance");
    let request = {
      origin: this.originAdd || this.ShippingForm.value.originAddress,
      destination: this.destAdd || this.ShippingForm.value.conAddress,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, (response, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        let distance = response.routes[0].legs[0].distance.value;
        console.log(distance);
        let num = distance / 1609.34;
        console.log(num);
        distanceInput.value = num.toFixed(2);
        this.OrderForm.value.totalDistance = distanceInput.value;
      } else {
        alert(`Please don't forget to fill in origin / consignee field`);
      }
    });
  }

  clickDistance() {
    let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
    el.click();
  }

  onSelectCard(event) {
    this.activeCard = event;
    console.log(this.activeCard);
  }

  approveReferral(id) {
    let sendToken = {
      referralId: id,
      source: this.activeCard.id
    };

    this.ordersvc.approveCharge(sendToken).subscribe(
      result => {
        console.log(result);
        alert("This referral request has been approved");
        this.router.navigateByUrl("/referrals");
      },
      err => {
        console.log(err);
      }
    );
  }

  rejectReferral(id) {
    this.ordersvc.rejectCharge(id).subscribe(data => console.log(data));
    alert("This referral request has been rejected.");
    this.router.navigateByUrl("/referrals");
  }

  onSelectChargeCard(event) {
    this.chargeActiveCard = event;
    console.log(this.chargeActiveCard);
  }

  getChargeToken() {
    const self = this;

    self.message = "Sending details...";
    alert(self.message);

    if (self.chargeActiveCard) {
      self.charge(self.chargeActiveCard.id);
    } else {
      (<any>window).Stripe.card.createToken(
        {
          number: self.chargeCardNumber,
          exp_month: self.chargeExpiryMonth,
          exp_year: self.chargeExpiryYear,
          cvc: self.chargeCvc,
          name: self.chargeCardName,
          address_line1: self.chargeBill_street,
          address_city: self.chargeBill_city,
          address_state: self.chargeBill_state,
          address_zip: self.chargeBill_zipcode
        },
        (status: number, response: any) => {
          self.ngZone.run(() => {
            if (status === 200) {
              self.charge(response.id);
            } else {
              self.message = response.error.message;
            }
          });
        }
      );
    }
  }

  charge(id) {
    const obj = {
      referralId: this.orderId,
      source: id,
      chargeAmount: this.chargeAmount
    };

    this.ordersvc.additionalCharge(obj).subscribe(
      result => {
        console.log(result);
        alert(`Partner's credit card has been charged`);
        this.resetModals();
      },
      err => {
        this.message = err.error.message;
        this.resetModals();
      }
    );
  }

  getRefundToken() {
    const self = this;

    self.message = "Sending details...";
    alert(self.message);

    if (self.chargeActiveCard) {
      self.refund(self.chargeActiveCard.id);
    } else {
      (<any>window).Stripe.card.createToken(
        {
          number: self.chargeCardNumber,
          exp_month: self.chargeExpiryMonth,
          exp_year: self.chargeExpiryYear,
          cvc: self.chargeCvc,
          name: self.chargeCardName,
          address_line1: self.chargeBill_street,
          address_city: self.chargeBill_city,
          address_state: self.chargeBill_state,
          address_zip: self.chargeBill_zipcode
        },
        (status: number, response: any) => {
          self.ngZone.run(() => {
            if (status === 200) {
              self.refund(response.id);
            } else {
              self.message = response.error.message;
            }
          });
        }
      );
    }
  }

  refund(id) {
    const obj = {
      referralId: this.order.id,
      source: id,
      refundAmount: this.refundAmount
    };
    this.ordersvc.refundPaidOrder(obj).subscribe(
      result => {
        console.log(result);
        alert(`Partner's credit card has been refunded`);
        this.resetModals();
      },
      err => {
        this.message = err.error.message;
        this.resetModals();
      }
    );
  }

  resetModals() {
    this.refundAmount = "";
    this.chargeAmount = "";
    this.chargeCardNumber = "";
    this.chargeExpiryMonth = "";
    this.chargeExpiryYear = "";
    this.chargeCvc = "";
    this.chargeCardName = "";
    this.autoaddress.nativeElement.value = "";
    this.chargeBill_street = "";
    this.chargeBill_city = "";
    this.chargeBill_state = "";
    this.chargeBill_zipcode = "";
    this.chargeSelectedCard = undefined;
    this.message = "";
  }
}
