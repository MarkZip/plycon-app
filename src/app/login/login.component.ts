import { Component} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {OrderService} from '../services/order.service';
import {Router} from '@angular/router'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
   emailadd;
   password;
   firstname;
   lastname;
   
   email;
   pwd;
   first;
   last;
   alert;
   
   isLoading: boolean;
   
   recoveryemail;
   

    constructor
	   (public router : Router,
	    public ordersvc : OrderService) 
	   {
       this.email = new FormControl('', [Validators.email, Validators.required]);
       this.pwd = new FormControl('', [Validators.required]);
	   this.first = new FormControl('', [Validators.required]);
	   this.last = new FormControl('', [Validators.required]);
    }
	
	
	  public loginUser() {
	  this.isLoading = true;
      this.ordersvc.login(this.emailadd, this.password)
	   .subscribe(
	    (response) => { 
		console.log(response);
		this.isLoading = false;
		this.router.navigateByUrl('/referrals')},
	    (error) => {
		this.isLoading = false;
		console.log(error);
		this.alert = "Login and/or password not recognized. Please try again or recover your login info";
		}
	   );
    }
	
		
  // public signUpUser() {
  //     this.ordersvc.signup(this.emailadd, this.password)
	//    .subscribe(
	//   (response) => {
	//   alert("Signup successful! Please check your email to activate your account")
	//   location.reload()
	//   },
	//   (error) => alert('Your details are not submitted. Please register again')
	//   );

       
	  
  // }
  
   recoverPassword(){
   
   this.ordersvc.forgotPassword(this.recoveryemail)
     .subscribe(
	  (response) => {
	  
	  console.log(response)
	  alert("Email sent! Please check your email")
	  
	  
	  
	  },
	  
	  (error) => {
	  
	   console.log(error)
	  alert("Enter a valid email")
	  }
	  
	  );
   
  }

}
