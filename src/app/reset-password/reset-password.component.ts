
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

 confirmpwd;
 newpwd;
 
 newpassword;
 confirmpassword;
 
 token:string;
 email:any;


  constructor(private router: Router, private ordersvc: OrderService, private route: ActivatedRoute) {
  
  this.newpwd = new FormControl('', [Validators.minLength(6), Validators.required]);
  this.confirmpwd = new FormControl('', [Validators.minLength(6), Validators.required]);
  
  }

  ngOnInit() {
  
     this.route.params.subscribe((params: Params) => {
        this.token = params['token'];
        console.log(this.token);
      });
  }
  
   resetPassword(){
    	this.ordersvc.resetPassword(this.token, this.newpassword)
  		.subscribe((data) => {
  			alert('Password changed! Please log in back to your account')
			this.router.navigateByUrl('/login')
		
  		}, (error) => {
  			console.log("Please enter a new password")
			this.router.navigateByUrl('/login')
			
			
  		});
  }
 
  
  

}
