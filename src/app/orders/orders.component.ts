import { Component, OnInit } from "@angular/core";
import { OrderService } from "../services/order.service";

import * as jwt_decode from "jwt-decode";

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"]
})
export class OrdersComponent implements OnInit {
  public orders: any;
  public ordersList: any;
  public partnerSearch: any;
  public filteredOrders = [];
  public totalCommissions: number;
  public totalCommissionsPending: number;
  public totalCommissionsPaid: number;
  public loading = false;

  userRole;

  constructor(private ordersvc: OrderService) {}

  ngOnInit() {
    this.getAccessToken();
    this.loading = true;
  }

  logout() {
    localStorage.clear();
  }

  getAccessToken() {
    var token = localStorage.getItem("accessToken");
    var decoded = jwt_decode(token);
    this.userRole = decoded.role;
    console.log(this.userRole);
    if (this.userRole === "superAdmin") {
      this.getAdminOrders();
    } else {
      this.getPartnerOrders();
    }
  }

  getAdminOrders() {
    this.ordersvc.adminGetOrders().subscribe(
      result => {
        console.log("superAdmin");
        this.orders = result;
        console.log(this.orders);
        this.filteredOrders = this.orders;
        this.ordersList = this.removeDuplicatePartnerNames(this.orders);
        this.getTotalCommissions(result);
        console.log(this.filteredOrders);
        this.loading = false;
      },
      err => {
        console.log(err);
      }
    );
  }

  removeDuplicatePartnerNames(data) {
    return data.filter((item, index, arr) => {
      const c = arr.map(item => item.user.name);
      return index === c.indexOf(item.user.name);
    });
  }

  getPartnerOrders() {
    this.ordersvc.getOrders().subscribe(
      result => {
        console.log("partner");
        this.orders = result;
        console.log(this.orders);
        this.filteredOrders = this.orders;
        this.ordersList = this.removeDuplicatePartnerNames(this.orders);
        this.getTotalCommissions(result);
        console.log(this.filteredOrders);
        this.loading = false;
      },
      err => {
        console.log(err);
      }
    );
  }

  getTotalCommissions(orders) {
    let totalComm = 0;
    let totalCommPend = 0;
    let totalCommPaid = 0;
    orders.map(order => {
      totalComm += (order.pq1 / 100) * order.transportationCharge;
      if (order.commissionStatus === "pending") {
        totalCommPend += (order.pq1 / 100) * order.transportationCharge;
      }
      if (order.commissionStatus === "paid") {
        totalCommPaid += (order.pq1 / 100) * order.transportationCharge;
      }

      this.totalCommissions = totalComm;
      this.totalCommissionsPending = totalCommPend;
      this.totalCommissionsPaid = totalCommPaid;
    });
  }

  selectPartner(event) {
    this.loading = true;
    if (this.partnerSearch === "Select all Partners") {
      this.getAdminOrders();
    } else {
      this.filteredOrders = this.orders.filter(
        (order: any) => order.user.name === this.partnerSearch
      );
      console.log(this.filteredOrders);
      this.loading = false;
    }
  }
}
