import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {OrderService} from '../services/order.service';
import { Router, Params, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})

export class ActivateComponent implements OnInit {

 isActivated:boolean = false;
 token:string;

  constructor(private router: Router, private ordersvc: OrderService, private route: ActivatedRoute) { }

  ngOnInit() {
  
     this.route.params.subscribe((params: Params) => {
        this.token = params['token'];
        console.log(this.token);
      });
  
  
  
  	this.ordersvc.activate(this.token)
  		.subscribe((data) => {
  			//alert('Your account has been activated')
			this.isActivated = true;
  		}, (error) => {
  			console.log(error)
			this.isActivated = false;
			
  		});
  
  
  }
  
    reactivateEmail(){
       this.ordersvc.reactivate()
	    .subscribe((data) => console.log(data));
}

}


  